package com.example.myrestwebservice

import retrofit2.Call
import retrofit2.http.*

interface WebService {

    @GET("getEncuestas.php")
    fun getEncuestas(): Call<MyResponseApi>
    //(@QueryMap params: HashMap<String, Any>)//(@Query("id") id: Int)
    @FormUrlEncoded
    @POST("insertAnswer")
    fun insertAnswer(@Body params: HashMap<String, Any>): Call<MyResponseApi>
}