package com.example.myrestwebservice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var retrofit: Retrofit
    private lateinit var webService: WebService
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        retrofit = Retrofit.Builder().baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create()).build()

        webService = retrofit.create(WebService::class.java)

        val callEncuestas = webService.getEncuestas()

        MyLoader.showLoader(this)
        Toast.makeText(this, "Sín internet", Toast.LENGTH_LONG).show()
        callEncuestas.enqueue(object: Callback<MyResponseApi>{
            override fun onFailure(call: Call<MyResponseApi>, t: Throwable) {
                Log.e("Algo", "errro ${t.message}")
            }

            override fun onResponse(call: Call<MyResponseApi>, response: Response<MyResponseApi>) {
                val responseApi = response.body()
                if (responseApi != null) {
                    if (responseApi.result) {

                    }
                }
                MyLoader.hideLpoader()
            }
        })
    }
}
