package com.example.myrestwebservice

import android.app.ProgressDialog

class MyLoader {
    companion object {
        private lateinit var progressDialog: ProgressDialog
        fun showLoader(activity: MainActivity) {
            progressDialog = ProgressDialog(activity)
            progressDialog.setTitle("Hola")
            progressDialog.setMessage("Cargando...")
            progressDialog.setCancelable(false)
            progressDialog.show()
        }

        fun hideLpoader() {
            progressDialog.dismiss()
        }
    }
}