package com.example.myrestwebservice

data class Encuesta(var id: Int, var question: String,
                    var answer1: String, var answer2: String,
                    var status: Boolean) {
    constructor(): this(0, "", "", "", false)

}