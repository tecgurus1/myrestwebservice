package com.example.myrestwebservice

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MyResponseApi {
    @Expose
    @SerializedName("result")
    var result = false

    @Expose
    @SerializedName("message")
    var msg = ""

    @Expose
    @SerializedName("encuestas")
    var listEncusta: MutableList<Encuesta> = mutableListOf()
}